#!/usr/bin/env python3

from sanic import Sanic
from sanic.response import json


app = Sanic()


@app.route('/api/v1/ping')
async def ping(request):
    return json({'response': 'pong'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
