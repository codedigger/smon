#!/usr/bin/env python3

from distutils.core import setup

setup(
    name='Smon',
    version='0.0',
    description='Supervisors monitor',
    author='Pawel Surowiec',
    author_email='pawel.surowiec@gmail.com',
    url='https://bitbucket.org/codedigger/smon',
    packages=[])
