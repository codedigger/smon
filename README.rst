####
SMON
####

Overview
########

SMON (supervisors monitor) is a simple monitor/management tool. It will allow to
- start and stop any supervosor process on any configured supervisor server
- monitor the process state

... and more (in the future).

This project is not designed to be run on production - it is in development state. Cam back later if you are
interrested in.

Setting up the project
======================

Backend files
-------------

Install all (or needed) requirements.<type>.txt files with ``pip3 install -r <file>``

Frontend virtualenvironment
---------------------------

First, prepare node virtual environment

.. code:: bash

   pip install nodeenv
   nodeenv -p
